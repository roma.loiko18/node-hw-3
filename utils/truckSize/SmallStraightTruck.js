const Truck = require('./Truck');

/**
 * Truck
 */
class SmallStraight extends Truck {
  /**
   * @constructor
   */
  constructor() {
    super(500, 250, 170, 2500);
  }
}

module.exports = new SmallStraight();
