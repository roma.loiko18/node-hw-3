const Truck = require('./Truck');

/**
 * Truck
 */
class LargeStraight extends Truck {
  /**
   * @constructor
   */
  constructor() {
    super(700, 350, 200, 4000);
  }
}

module.exports = new LargeStraight();
