const Truck = require('./Truck');

/**
 SprinterTruck
 */
class SprinterTruck extends Truck {
  /**
     * @constructor
     */
  constructor() {
    super(300, 250, 170, 1700);
  }
}

module.exports = new SprinterTruck();
