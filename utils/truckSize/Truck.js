/**
 * Truck
 */
class Truck {
  /**
     * @constructor
     * @param {number} width
     * @param {number} length
     * @param {number} height
     * @param {number} payload
     */
  constructor(width, length, height, payload) {
    this.width = width;
    this.length = length;
    this.height = height;
    this.payload = payload;
  }


  /**
     * validator
     * @param {object} load
     * @return {boolean}
     */
  validateDimensions(load) {
    return load.dimensions.width < this.width &&
            load.dimensions.height < this.height &&
            load.dimensions.length < this.length &&
            load.payload < this.payload;
  }
}

module.exports = Truck;
