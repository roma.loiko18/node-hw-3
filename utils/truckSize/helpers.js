const {truckType, truckTypes} = require('../../constants/truck');
const Sprinter = require('./SprinterTruck');
const LargeStraight = require('./LargeStraightTruck');
const SmallStraight = require('./SmallStraightTruck');


/**
 * @param {string} type
 * @return {object}
  */
function getTruckByType(type) {
  if (type === truckType.SPRINTER) return Sprinter;
  if (type === truckType.LARGE_STRAIGHT) return LargeStraight;
  if (type === truckType.SMALL_STRAIGHT) return SmallStraight;
  return null;
}

/**
 * @param {object} load
 * @return {object}
 */
function getTruckType(load) {
  for (const truck of truckTypes) {
    const type = getTruckByType(truck);
    if (type.validateDimensions(load)) {
      return truck;
    }
  }
  return null;
}

module.exports = {getTruckType};
