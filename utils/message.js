const message = (msg) => {
  return {message: msg};
};

const logRequest = (request) => {
  console.log(
      `${request.method}/${request.url}/${request.body.toString() || 'empty'}`,
  );
};


module.exports = {message, logRequest};
