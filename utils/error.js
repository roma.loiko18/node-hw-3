const {message} = require('./message');
const {messages} = require('../constants/message');


/**
 * A class that can implements custom error
 */
class BadRequestError extends Error {
  /**
     * @constructor
     * @param {string} message
     * @param {number} status
     */
  constructor(message = 'Bad Request!', status = 400) {
    super(message);
    this.status = status;
  }
}

const errorResponse = (error, res) => {
  if (error instanceof BadRequestError) {
    return res.status(400).send(message(error.message));
  } else {
    return res.status(500).send(message(messages.SERVER_ERROR));
  }
};

module.exports = {BadRequestError, errorResponse};
