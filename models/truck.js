const mongoose = require('mongoose');
const {truckType, truckStatus} = require('../constants/truck');

const truckSchema = new mongoose.Schema({
  type: {
    type: String,
    enum: [
      truckType.SPRINTER,
      truckType.SMALL_STRAIGHT,
      truckType.LARGE_STRAIGHT,
    ],
    required: true,
  },
  status: {
    type: String,
    enum: [truckStatus.OL, truckStatus.IS],
    default: truckStatus.IS,
  },
  payload: {
    type: Number,
  },
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    default: null,
  },
  dimensions: Object,
  created_date: {
    type: Date,
    default: Date.now(),
  },

});

const Truck = mongoose.model('Truck', truckSchema);


module.exports = Truck;
