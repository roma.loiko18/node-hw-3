require('dotenv').config();
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const Note = require('./truck');
const {userRole} = require('../constants/user');
const {BadRequestError} = require('../utils/error');

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    trim: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
    trim: true,
  },
  role: {
    type: String,
    enum: [userRole.DRIVER, userRole.SHIPPER],
    required: true,
  },
  tokens: [{
    token: {
      type: String,
      required: true,
    },
  }],
  avatar: {
    type: Buffer,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },

},
);


userSchema.methods.generateAuthToken = async function() {
  const token = jwt.sign({_id: this._id.toString()}, process.env.SECRET);
  this.tokens = this.tokens.concat({token});
  await this.save();
  return token;
};

userSchema.statics.findByCredentials = async (email, password) => {
  const user = await User.findOne({email});
  if (!user) {
    throw new BadRequestError(
        'Unable to login! (no such user with such email!)',
    );
  }

  const isMatch = await bcrypt.compare(password, user.password);
  if (!isMatch) {
    throw new BadRequestError('Unable to login!');
  }

  return user;
};

userSchema.pre('save', async function(next) {
  if (this.isModified('password')) {
    console.log(this.password);
    this.password = await bcrypt.hash(this.password, 8);
  }
  next();
});

userSchema.methods.toJSON = function() {
  const user = this.toObject();

  delete user.password;
  delete user.tokens;

  return user;
};


userSchema.pre('remove', async function(next) {
  await Note.deleteMany({userId: this._id});
  next();
});


const User = mongoose.model('User', userSchema);


module.exports = User;
