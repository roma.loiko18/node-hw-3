const mongoose = require('mongoose');
const {loadStatus, loadState} = require('../constants/load');

const loadSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    enum: [
      loadStatus.NEW,
      loadStatus.POSTED,
      loadStatus.ASSIGNED,
      loadStatus.SHIPPED,
    ],
    default: loadStatus.NEW,
    required: false,
  },
  state: {
    type: String,
    enum: [
      loadState.EN_ROUTE_TO_PICK_UP,
      loadState.ARRIVED_TO_PICK_UP,
      loadState.EN_ROUTE_TO_DELIVERY,
      loadState.ARRIVED_TO_DELIVERY,
    ],
    required: false,
    default: loadState.EN_ROUTE_TO_PICK_UP,
  },
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: false,
    ref: 'User',
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  logs: [
    {
      message: {
        type: String,
        default: `Load assigned to driver with id ###`,
      },
      time: {
        type: Date,
        default: Date.now(),
      },
    },
  ],
  createdDate: {
    type: Date,
    default: Date.now(),
  },
},
);

const Load = mongoose.model('Load', loadSchema);

module.exports = Load;
