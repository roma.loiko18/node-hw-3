require('./db/mongoose');
require('dotenv').config();
const cors = require('cors');

const express = require('express');
const userRouter = require('./routers/user');
const truckRouter = require('./routers/truck');
const loadRouter = require('./routers/load');


const app = express();
const port = process.env.PORT || 8080;


app.use(express.json());
app.use(cors());


app.use(userRouter);
app.use(truckRouter);
app.use(loadRouter);


app.listen(port, () => {
  console.log(`Server is up on port: ${port}`);
});

