const loadStatus = {
  NEW: 'NEW',
  POSTED: 'POSTED',
  ASSIGNED: 'ASSIGNED',
  SHIPPED: 'SHIPPED',
};

const loadState = {
  EN_ROUTE_TO_PICK_UP: 'En route to Pick Up',
  ARRIVED_TO_PICK_UP: 'Arrived to Pick Up',
  EN_ROUTE_TO_DELIVERY: 'En route to delivery',
  ARRIVED_TO_DELIVERY: 'Arrived to delivery',
};

module.exports = {
  loadState, loadStatus,
};
