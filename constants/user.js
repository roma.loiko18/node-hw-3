const userRole = {
  DRIVER: 'DRIVER',
  SHIPPER: 'SHIPPER',
};

module.exports = {userRole};
