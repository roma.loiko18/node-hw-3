const truckType = {
  SPRINTER: 'SPRINTER',
  SMALL_STRAIGHT: 'SMALL STRAIGHT',
  LARGE_STRAIGHT: 'LARGE STRAIGHT',
};

const truckTypes = [
  truckType.SPRINTER,
  truckType.SMALL_STRAIGHT,
  truckType.LARGE_STRAIGHT,
];

const truckStatus = {
  OL: 'OL',
  IS: 'IS',
};

module.exports = {
  truckStatus, truckType, truckTypes,
};
