const messages = {
  SUCCESS: 'Success',
  SERVER_ERROR: 'Server Error',
  BAD_REQUEST: 'Bad request',
  NO_SUCH_USER: 'No such user exists',
  USER_EXISTS: 'User already exists',
  NO_SUCH_NOTE: 'No such note exists',
  INVALID_OPTIONS: 'Invalid options',
  INVALID_PARAMS: 'Invalid params',
  PASSWORDS_DO_NOT_MATCH: 'Passwords don`t match',
  NO_NEW_PASSWORD_OCCURS: 'No new password occurs',
};


module.exports = {messages};
