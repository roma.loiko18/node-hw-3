require('dotenv').config();

const mongoose = require('mongoose');

const connectionUrl = process.env.CONNECTION_URL;

mongoose.connect(
    connectionUrl,
    {useNewUrlParser: true, useUnifiedTopology: true},
    () => console.log(' Mongoose is connected'),
);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error: '));
db.once('open', function() {
  console.log('Connected successfully');
});
