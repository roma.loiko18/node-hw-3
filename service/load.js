const Load = require('../models/load');
const Truck = require('../models/truck');
const {BadRequestError} = require('../utils/error');
const {userRole} = require('../constants/user');
const {loadStatus, loadState} = require('../constants/load');
const {truckStatus} = require('../constants/truck');
const {getTruckType} = require('../utils/truckSize/helpers');

const getLoadsForUser = async (
    userId,
    role,
    limit = 10,
    offset = 0,
    status = {$ne: null},
) => {
  try {
    if (role === userRole.SHIPPER) {
      const loads = await Load.find({created_by: userId})
          .limit(parseInt(limit))
          .skip(parseInt(offset));
      return loads;
    }
    if (role === userRole.DRIVER) {
      const activeLoad = await Load.findOne({
        assigned_to: userId,
        status: loadStatus.ASSIGNED,
      })
          .limit(parseInt(limit))
          .skip(parseInt(offset));
      const completedLoads = await Load.find({
        assigned_to: userId,
        status: loadStatus.SHIPPED,
      })
          .limit(parseInt(limit))
          .skip(parseInt(offset));
      return [activeLoad, ...completedLoads];
    }
  } catch (error) {
    throw new BadRequestError();
  }
}
;

const addLoadToShipper = async (shipperId, loadPayload) => {
  try {
    const load = new Load({...loadPayload, shipperId});
    await load.save();
  } catch (e) {
    throw new BadRequestError();
  }
};

const getActiveLoadForDriver = async (driverId) => {
  const activeLoad = await Load.findOne({
    assigned_to: driverId,
    status: loadStatus.ASSIGNED,
  });

  if (!activeLoad) {
    throw new BadRequestError('No active load');
  }

  return activeLoad;
};


const postShipperLoad = async (loadId, shipperId) => {
  const load = await Load.findOne(
      {_id: loadId, created_by: shipperId, status: loadStatus.NEW},
      {updatedAt: 0, __v: 0});

  if (!load) {
    throw new BadRequestError(`No load with such id found`);
  }

  await load.updateOne({
    $set: {
      status: loadStatus.POSTED,
    },
    $push: {
      logs: {
        message: `Load status changed to 'POSTED'`,
        time: new Date(Date.now()).toISOString(),
      },
    },
  });

  let driverFound = false;
  let message = 'Driver not found';
  const type = getTruckType(load);
  console.log(type);
  if (type) {
    const truck = await Truck.findOne({
      status: truckStatus.IS,
      assigned_to: {$ne: null},
      type,
    });

    if (truck) {
      await load.updateOne({
        $set: {
          status: loadStatus.ASSIGNED,
          state: loadState.EN_ROUTE_TO_PICK_UP,
          assigned_to: truck.assigned_to,
        },
      });
      await truck.updateOne({
        status: truckStatus.OL,
      });
      message = 'Driver found';
      driverFound = true;
    }
  }

  if (!driverFound) {
    await load.updateOne({
      $set: {
        status: loadStatus.NEW,
      },
      $push: {
        logs: {
          message,
        },
      },
    });
  }

  return {
    message,
    driver_found: driverFound,
  };
};

const iterateLoadState = async (driverId) => {
  const load = await Load.findOne({
    assigned_to: driverId,
    status: 'ASSIGNED',
  });

  if (!load) {
    throw new BadRequestError('no such load');
  }

  const state = getNextState(load.state);

  if (state === 'Arrived to delivery') {
    await load.updateOne({
      $set: {
        state,
        status: 'SHIPPED',
      },
      $push: {
        logs: {
          message: `Load state is changed to ${state}`,
          time: new Date(Date.now()).toISOString(),
        },
      },
    });

    await Truck.findOneAndUpdate(
        {created_by: load.assigned_to, status: 'OL'},
        {
          $set: {
            status: 'IS',
          },
        });
  } else {
    await load.updateOne({
      $set: {
        state,
      },
      $push: {
        logs: {
          message: `Load state is changed to ${state}`,
          time: new Date(Date.now()).toISOString(),
        },
      },
    });
  }
  return load.state;
};

/**
 *
 * @param currentState
 * @return {string}
 */
function getNextState(currentState) {
  const states = [
    loadState.EN_ROUTE_TO_PICK_UP,
    loadState.ARRIVED_TO_PICK_UP,
    loadState.EN_ROUTE_TO_DELIVERY,
    loadState.ARRIVED_TO_DELIVERY,
  ];
  const id = states.indexOf(currentState);
  if (states[id + 1]) {
    return states[id + 1];
  }
}


const getLoadByIdForUser = async (loadId, userId) => {
  const load = await Load.findOne({_id: loadId, created_by: userId});
  if (!load) {
    throw new BadRequestError('No load with such id found');
  }
  return load;
};

const updateLoadByIdForUser = async (loadId, userId, data) => {
  const load = await Load.findOne({_id: loadId, created_by: userId});

  if (!load) {
    throw new BadRequestError('No load with such id found');
  }

  if (load.status !== loadStatus.NEW) {
    throw new BadRequestError('You can\'t update this load');
  }

  await load.updateOne({$set: data});
};

const deleteLoadByIdForUser = async (loadId, userId) => {
  const load = await Load.findOne({_id: loadId, created_by: userId});

  if (!load) {
    throw new BadRequestError('No load with such id found');
  }

  if (load.status !== loadStatus.NEW) {
    throw new BadRequestError('You can\'t delete this load');
  }

  await load.remove();
};

const getShippingInfoByIdForUser = async (loadId, userId) => {
  const load = await Load.findOne(
      {_id: loadId, created_by: userId, status: loadStatus.ASSIGNED},
  );

  if (!load) {
    throw new BadRequestError('No load with such id found');
  }

  const truck = await Truck.findOne({
    assigned_to: load.assigned_to,
  });

  return {load, truck};
};


module.exports = {
  getLoadsForUser,
  addLoadToShipper,
  getActiveLoadForDriver,
  postShipperLoad,
  iterateLoadState,
  getLoadByIdForUser,
  updateLoadByIdForUser,
  deleteLoadByIdForUser,
  getShippingInfoByIdForUser,
  getNextState,
};
