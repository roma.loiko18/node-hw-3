'use strict';

const Truck = require('../models/truck');
const {BadRequestError} = require('../utils/error');
const {truckType, truckStatus} = require('../constants/truck');


const getDriverTrucksByDriverId = async (userId) => {
  const truck = await Truck.find({created_by: userId});
  if (!truck) {
    throw new BadRequestError('No trucks yet');
  }
  return truck;
};

const addTruckToDriver = async (driverId, truckPayload) => {
  if (truckPayload.type === truckType.SPRINTER) {
    truckPayload.payload = 1700;
    truckPayload.dimensions = {
      width: 300,
      length: 250,
      height: 170,
    };
    const truck = new Truck({...truckPayload, created_by: driverId});

    await truck.save();
  }

  if (truckPayload.type === truckType.SMALL_STRAIGHT) {
    truckPayload.payload = 2500;
    truckPayload.dimensions = {
      width: 500,
      length: 250,
      height: 170,
    };
    const truck = new Truck({...truckPayload, driverId});

    await truck.save();
  }

  if (truckPayload.type === truckType.LARGE_STRAIGHT) {
    truckPayload.payload = 4000;
    truckPayload.dimensions = {
      width: 700,
      length: 350,
      height: 200,
    };
    const truck = new Truck({...truckPayload, driverId});
    await truck.save();
  }
};

const getTruckByIdForDriver = async (truckId, driverId) => {
  const truck = await Truck.findOne({_id: truckId, created_by: driverId});

  if (!truck) {
    throw new BadRequestError();
  }

  return truck;
};

const updateTruckByIdForDriver = async (truckId, driverId, type) => {
  const truck = await Truck.findOne({_id: truckId, created_by: driverId});

  if (!truck) {
    throw new BadRequestError();
  }

  if (truck.assigned_to) {
    throw new BadRequestError();
  }

  await truck.updateOne({$set: {type}});
};

const deleteTruckByIdForDriver = async (truckId, driverId) => {
  const truck = await Truck.findOne({_id: truckId, created_by: driverId});

  if (!truck) {
    throw new BadRequestError('No truck with such id found');
  }

  if (truck.assigned_to) {
    throw new BadRequestError(
        `You can\'t update truck info, while it assigned`);
  }

  await truck.remove();
};

const assignTruckByIdForDriver = async (truckId, driverId) => {
  const assignedTruck = await Truck.findOne(
      {created_by: driverId, assigned_to: driverId},
  );

  if (assignedTruck && assignedTruck.status === truckStatus.OL) {
    throw new BadRequestError();
  }

  await Truck.findOneAndUpdate(
      {created_by: driverId, assigned_to: driverId},
      {$set: {assigned_to: null}},
  );

  const truck = await Truck.findOneAndUpdate(
      {_id: truckId, created_by: driverId},
      {$set: {assigned_to: driverId}},
  );

  if (!truck) {
    throw new BadRequestError();
  }
};

module.exports = {
  getDriverTrucksByDriverId,
  addTruckToDriver,
  getTruckByIdForDriver,
  updateTruckByIdForDriver,
  deleteTruckByIdForDriver,
  assignTruckByIdForDriver,
};
