const express = require('express');
const auth = require('../middleware/auth');
const {message, logRequest} = require('../utils/message');
const {driverRole} = require('../middleware/role');
const {
  getDriverTrucksByDriverId,
  addTruckToDriver,
  getTruckByIdForDriver,
  updateTruckByIdForDriver, deleteTruckByIdForDriver, assignTruckByIdForDriver,
} = require('../service/truck');
const {errorResponse} = require('../utils/error');
const {checkTruckType} = require('../middleware/validation');
const {messages} = require('../constants/message');
const {truckType} = require('../constants/truck');

const router = new express.Router();

router.get('/api/trucks', auth, driverRole, async (req, res) => {
  logRequest(req);
  const driverId = req.user._id;
  try {
    console.log(driverId);
    const trucks = await getDriverTrucksByDriverId(driverId);
    res.status(200).send({trucks});
  } catch (e) {
    errorResponse(e, res);
  }
});

router.post('/api/trucks',
    auth,
    driverRole,
    checkTruckType,
    async (req, res) => {
      logRequest(req);

      const validTruckTypes = [
        truckType.SPRINTER,
        truckType.SMALL_STRAIGHT,
        truckType.LARGE_STRAIGHT,
      ];
      const driverId = req.user._id;
      const type = req.body.type;
      if (!validTruckTypes.includes(type)) {
        return res.status(400).send(message(messages.BAD_REQUEST));
      }
      try {
        const truck = {
          type: type,
          created_by: driverId,
        };

        await addTruckToDriver(driverId, truck);
        res.status(200).send(message('Truck created successfully'));
      } catch (e) {
        errorResponse(e, res);
      }
    });

router.get('/api/trucks/:id', auth, driverRole, async (req, res) => {
  logRequest(req);

  const truckId = req.params.id;
  const driverId = req.user._id;
  try {
    const truck = await getTruckByIdForDriver(truckId, driverId);
    res.status(200).send({truck});
  } catch (e) {
    errorResponse(e, res);
  }
});

router.delete('/api/trucks/:id', auth, driverRole, async (req, res) => {
  logRequest(req);

  const driverId = req.user.id;
  const truckId = req.params.id;
  try {
    await deleteTruckByIdForDriver(truckId, driverId);
    res.status(200).send(message('Truck deleted successfully'));
  } catch (e) {
    errorResponse(e, res);
  }
});

router.put('/api/trucks/:id',
    auth,
    driverRole,
    checkTruckType,
    async (req, res) => {
      logRequest(req);

      const driverId = req.user._id;
      const truckId = req.params.id;
      const truckType = req.body.type;

      try {
        await updateTruckByIdForDriver(truckId, driverId, truckType);
        res.status(200).send(message( 'Truck details changed successfully'));
      } catch (e) {
        errorResponse(e, res);
      }
    });

router.post('/api/trucks/:id/assign', auth, driverRole, async (req, res) => {
  logRequest(req);

  const truckId = req.params.id;
  const driverId = req.user._id;
  try {
    await assignTruckByIdForDriver(truckId, driverId);
    res.status(200).send(message('Truck assigned successfully'));
  } catch (e) {
    errorResponse(e, res);
  }
});

module.exports = router;
