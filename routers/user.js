const User = require('../models/user');
const express = require('express');
const auth = require('../middleware/auth');
const {message, logRequest} = require('../utils/message');
const {validRole} = require('../middleware/role');
const multer = require('multer');
const sharp = require('sharp');
const {checkPassword, checkRegister} = require('../middleware/validation');
const {messages} = require('../constants/message');
const {errorResponse, BadRequestError} = require('../utils/error');
const bcrypt = require('bcryptjs');
const nodemailer = require('nodemailer');

const router = new express.Router();

const upload = multer({
  limits: {
    fileSize: 10000000,
  },
  fileFilter(req, file, cb) {
    if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
      return cb(new Error('Please upload an image'));
    }
    cb(undefined, true);
  },
});

router.post('/api/auth/register',
    validRole,
    checkRegister,
    async (req, res) => {
      logRequest(req);
      try {
        const user = new User(req.body);
        await user.save();
        res.status(200).send(message('Profile created successfully'));
      } catch (e) {
        errorResponse(e, res);
      }
    });

router.post('/api/auth/login', async (req, res) => {
  logRequest(req);

  try {
    const user =
            await User.findByCredentials(req.body.email, req.body.password);
    if (!user) {
      return res.status(400).send(message(messages.NO_SUCH_USER));
    }
    const token = await user?.generateAuthToken();
    res.status(200).send({jwt_token: token});
  } catch (error) {
    errorResponse(error, res);
  }
});

router.get('/api/users/me',
    auth,
    async (req, res) => {
      logRequest(req);
      res.status(200).send({user: req.user});
    });

router.patch('/api/users/me/password',
    auth,
    checkPassword,
    async (req, res) => {
      logRequest(req);

      const newPassword = req.body.newPassword;
      const oldPassword = req.body.oldPassword;
      const userId = req.user._id;
      try {
        const user = await User.findOne({_id: userId});
        const isValid = await bcrypt.compare(oldPassword, user.password);
        if (!isValid) {
          return res.status(400).send(
              message(messages.PASSWORDS_DO_NOT_MATCH),
          );
        }
        user.password = newPassword;
        await user.save();
        res.status(200).send(
            message('Password has been changed successfully!'),
        );
      } catch (error) {
        errorResponse(error, res);
      }
    });

router.post('/users/me/photo',
    auth,
    upload.single('avatar'),
    async (req, res) => {
      logRequest(req);

      const buffer =
            await sharp(req.file.buffer)
                .resize({width: 250, height: 250})
                .png()
                .toBuffer();
      console.log(buffer);
      req.user.avatar = req.file.buffer;
      await req.user.save();
      res.status(200).send(message('photo has been added successfully'));
    },
    (error, req, res, next) => {
      errorResponse(error, res);
    },
);

router.post('/api/me/logout', auth, async (req, res) => {
  logRequest(req);

  try {
    req.user.tokens = req.user.tokens.filter(
        (token) => token.token !== req.token,
    );
    await req.user.save();
    res.status(200).send(message(messages.SUCCESS));
  } catch (error) {
    errorResponse(error, res);
  }
});

router.delete('/api/users/me', auth, async (req, res) => {
  logRequest(req);

  try {
    await req.user.remove();
    res.send(req.user);
  } catch (error) {
    errorResponse(error, res);
  }
});


router.post('/api/auth/forgot_password', async (req, res) => {
  logRequest(req);
  const newPassword = 'qwerty12345';

  try {
    const {email} = req.body;
    if (!email) {
      return new BadRequestError();
    }
    const user = await User.findOne({email});
    if (!user) {
      return new BadRequestError();
    }
    user.password = newPassword;
    await user.save();

    const testAccount = await nodemailer.createTestAccount();
    const transporter = nodemailer.createTransport({
      host: 'smtp.ethereal.email',
      port: 587,
      secure: false,
      auth: {
        user: testAccount.user,
        pass: testAccount.pass,
      },
    });
    const info = await transporter.sendMail({
      from: '"Fred Foo 👻" <foo@example.com>',
      to: `${email}`,
      subject: 'Hello ✔',
      text: 'Request to generate new password?',
      html: '<b>New Password: qwerty12345</b>',
    });
    console.log('Message sent: %s', info.messageId);
    res.status(200).send(message('New password sent to your email address'));
  } catch (error) {
    errorResponse(error, res);
  }
});


module.exports = router;


