const express = require('express');
const auth = require('../middleware/auth');
const {message, logRequest} = require('../utils/message');
const {shipperRole, driverRole, validRole} = require('../middleware/role');
const {
  getLoadsForUser,
  addLoadToShipper,
  postShipperLoad,
  iterateLoadState,
  getLoadByIdForUser,
  updateLoadByIdForUser,
  deleteLoadByIdForUser,
  getShippingInfoByIdForUser,
  getActiveLoadForDriver,
} = require('../service/load');
const {errorResponse} = require('../utils/error');
const {checkLoad} = require('../middleware/validation');
const {messages} = require('../constants/message');
const router = new express.Router();


router.get('/api/loads', auth, validRole, async (req, res) => {
  logRequest(req);

  const userId = req.user._id;
  const userRole = req.user.role;

  const {limit, offset, status} = req.query;
  try {
    const loads =
            await getLoadsForUser(userId, userRole, limit, offset, status);
    res.status(200).send({loads});
  } catch (e) {
    errorResponse(e, res);
  }
});

router.post('/api/loads', auth, shipperRole, checkLoad, async (req, res) => {
  logRequest(req);

  const shipperId = req.user._id;
  const load = {
    ...req.body,
    created_by: shipperId,
  };
  try {
    await addLoadToShipper(shipperId, load);
    res.status(200).send(message(messages.SUCCESS));
  } catch (e) {
    errorResponse(e, res);
  }
});

router.get('/api/loads/active', auth, driverRole, async (req, res) => {
  logRequest(req);

  const userId = req.user._id;
  try {
    const load = await getActiveLoadForDriver(userId);
    res.status(200).send({load});
  } catch (e) {
    errorResponse(e, res);
  }
});

router.get('/api/loads/:id', auth, validRole, async (req, res) => {
  logRequest(req);

  const loadId = req.params.id;
  const userId = req.user._id;

  try {
    const load = await getLoadByIdForUser(loadId, userId);
    res.status(200).send({load});
  } catch (e) {
    errorResponse(e, res);
  }
});

router.patch('/api/loads/active/state', auth, driverRole, async (req, res) => {
  logRequest(req);

  const driverRole = req.user._id;
  try {
    const loadState = await iterateLoadState(driverRole);
    res.status(200).send(message(`Load state changed to ${loadState}`));
  } catch (e) {
    errorResponse(e, res);
  }
});

router.delete('/api/loads/:id', auth, shipperRole, async (req, res) => {
  logRequest(req);

  const shipperId = req.user._id;
  const loadId = req.params.id;

  try {
    await deleteLoadByIdForUser(loadId, shipperId);
    res.status(200).send(message('Load deleted successfully'));
  } catch (e) {
    errorResponse(e, res);
  }
});

router.put('/api/loads/:id', auth, shipperRole, checkLoad, async (req, res) => {
  logRequest(req);

  const loadId = req.params.id;
  const shipperId = req.user._id;
  try {
    await updateLoadByIdForUser(loadId, shipperId, req.body);
    res.status(200).send(message('Load deleted successfully'));
  } catch (e) {
    errorResponse(e, res);
  }
});

router.post('/api/loads/:id/post', auth, shipperRole, async (req, res) => {
  logRequest(req);

  const shipperId = req.user._id;
  const loadId = req.params.id;

  try {
    const response = await postShipperLoad(loadId, shipperId);
    res.status(200).send(response);
  } catch (e) {
    errorResponse(e, res);
  }
});

router.get('/api/loads/:id/shipping_info',
    auth,
    shipperRole,
    async (req, res) => {
      logRequest(req);

      const shipperId = req.user._id;
      const loadId = req.params.id;

      try {
        const response = await getShippingInfoByIdForUser(loadId, shipperId);
        res.status(200).send(response);
      } catch (e) {
        errorResponse(e, res);
      }
    });


module.exports = router;
