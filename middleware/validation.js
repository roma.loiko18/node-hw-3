const Joi = require('joi');
const {BadRequestError} = require('../utils/error');
const {userRole} = require('../constants/user');
const {truckType} = require('../constants/truck');
const {message} = require('../utils/message');
const {messages} = require('../constants/message');


const checkRegister = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .email()
        .required(),
    password: Joi.string()
        .required(),
    role: Joi.string()
        .valid(userRole.DRIVER, userRole.SHIPPER)
        .required(),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    res.status(400).send(message(messages.INVALID_OPTIONS));
  }
};

const checkPassword = async (req, res, next) => {
  const schema = Joi.object({
    oldPassword: Joi.string()
        .required(),
    newPassword: Joi.string()
        .required(),
  });

  try {
    await schema.validateAsync(req.body);
    console.log(true);
    next();
  } catch (err) {
    next(new BadRequestError(err.message));
  }
};

const checkTruckType = async (req, res, next) => {
  const schema = Joi.object({
    type: Joi.string()
        .valid(
            truckType.SPRINTER,
            truckType.SMALL_STRAIGHT,
            truckType.LARGE_STRAIGHT,
        )
        .required(),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    res.status(400).send(message(messages.INVALID_OPTIONS));
  }
};

const checkLoad = async (req, res, next) => {
  const schema = Joi.object({
    name: Joi.string()
        .required(),
    payload: Joi.number()
        .positive()
        .required(),
    pickup_address: Joi.string()
        .required(),
    delivery_address: Joi.string()
        .required(),
    dimensions: Joi.object({
      width: Joi.number(),
      length: Joi.number(),
      height: Joi.number(),
    })
        .required(),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    res.status(400).send(message(messages.INVALID_OPTIONS));
  }
};

module.exports = {
  checkTruckType,
  checkLoad,
  checkRegister,
  checkPassword,
};
