const {userRole} = require('../constants/user');

const driverRole = (req, res, next) => {
  try {
    const {role} = req.user;
    console.log(role);
    if (role !== userRole.DRIVER) {
      throw new Error();
    }
    next();
  } catch (error) {
    res.status(400).send({message: 'Please auth as a DRIVER!'});
  }
};

const shipperRole = (req, res, next) => {
  try {
    const {role} = req.user;
    if (role !== userRole.SHIPPER) {
      throw new Error();
    }
    next();
  } catch (error) {
    res.status(400).send({message: 'Please auth as a SHIPPER!'});
  }
};

const validRole = (req, res, next) => {
  try {
    const {role} = req.user ?? req.body;
    if (role !== userRole.SHIPPER && role !== userRole.DRIVER) {
      throw new Error();
    }
    next();
  } catch (error) {
    res.status(400).send({message: 'Please auth as a SHIPPER or DRIVER!'});
  }
};


module.exports = {driverRole, shipperRole, validRole};
